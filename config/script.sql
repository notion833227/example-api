create table if not exists cars(
    id serial primary key,
    manufacturer varchar(20),
    design varchar(20),
    style varchar(20),
    doors int,
    created_at timestamp default null,
    updated_at timestamp default null
);

create table if not exists users(
    id serial primary key,
    username varchar(20) unique,
    email varchar(40) unique,
    roles jsonb,
    password varchar(255),
    created_at timestamp default null,
    updated_at timestamp default null
);