package config

import (
	"golang.org/x/crypto/bcrypt"
	"regexp"
)

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func ValidePassword(password string) bool {
	listRegex := []string{
		`[0-9]`,
		`[a-z]`,
		`[A-Z]`,
		`[@$!%*?&]`,
		`^.{8,64}$`,
	}

	isValide := false
	for _, regex := range listRegex {
		isValide = regexp.MustCompile(regex).MatchString(password)
	}

	return isValide
}
