package main

import (
	"example-api/config"
	"example-api/controllers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"os"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
		return
	}

	config.DatabaseInit()
	log.Fatal(http.ListenAndServe(":"+os.Getenv("APP_PORT"), InitializeRouter()))
}

func InitializeRouter() *mux.Router {
	// StrictSlash is true => redirect /cars/ to /cars
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/login", config.Chain(controllers.Login, config.Logger(), config.RequestHeaders())).Methods("POST")
	router.HandleFunc("/register", config.Chain(controllers.Register, config.Logger(), config.RequestHeaders())).Methods("POST")
	router.HandleFunc("/cars", config.Chain(controllers.CarsIndex, config.Logger(), config.RequestHeaders())).Methods("GET")
	router.HandleFunc("/cars", config.Chain(controllers.CarsCreate, config.Logger(), config.RequestHeaders(), config.Auth(""))).Methods("POST")
	router.HandleFunc("/cars/{id}", config.Chain(controllers.CarsShow, config.Logger(), config.RequestHeaders())).Methods("GET")
	router.HandleFunc("/cars/{id}", config.Chain(controllers.CarsUpdate, config.Logger(), config.RequestHeaders(), config.Auth(""))).Methods("PUT")
	router.HandleFunc("/cars/{id}", config.Chain(controllers.CarsDelete, config.Logger(), config.RequestHeaders(), config.Auth(""))).Methods("DELETE")

	return router
}
