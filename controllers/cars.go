package controllers

import (
	"encoding/json"
	"example-api/models"
	"github.com/gorilla/mux"
	"io"
	"log"
	"net/http"
	"strconv"
)

func CarsIndex(w http.ResponseWriter, r *http.Request) {
	cars, err := models.AllCars()

	if err != nil {
		http.Error(w, "Unable to return all cars", http.StatusNotFound)
		return
	}

	json.NewEncoder(w).Encode(cars)
}

func CarsCreate(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)

	if err != nil {
		http.Error(w, "Unable to read request body", http.StatusBadRequest)
		return
	}

	var car models.Car
	err = json.Unmarshal(body, &car)

	if err != nil {
		log.Println(err)
		http.Error(w, "Unable to parse data", http.StatusUnprocessableEntity)
		return
	}

	models.NewCar(&car)

	json.NewEncoder(w).Encode(car)
}

func CarsShow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		http.Error(w, "Unable to read request body", http.StatusBadRequest)
		return
	}

	car, err := models.FindCarById(id)

	if err != nil {
		http.Error(w, "Car not found", http.StatusNotFound)
		return
	}

	json.NewEncoder(w).Encode(car)
}

func CarsUpdate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		http.Error(w, "Unable to read request body", http.StatusBadRequest)
		return
	}

	body, err := io.ReadAll(r.Body)

	if err != nil {
		http.Error(w, "Unable to read request body", http.StatusBadRequest)
		return
	}

	car, err := models.FindCarById(id)

	if err != nil {
		http.Error(w, "Car not found", http.StatusNotFound)
		return
	}

	if err = json.Unmarshal(body, &car); err != nil {
		http.Error(w, "Unable to parse data", http.StatusUnprocessableEntity)
		return
	}

	if err := models.UpdateCar(car); err != nil {
		http.Error(w, "Car not update", http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(car)
}

func CarsDelete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		http.Error(w, "Unable to read request body", http.StatusBadRequest)
		return
	}

	if err := models.DeleteCarById(id); err != nil {
		http.Error(w, "Car not deleted", http.StatusInternalServerError)
		return
	}
}
