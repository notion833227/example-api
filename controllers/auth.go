package controllers

import (
	"encoding/json"
	"example-api/config"
	"example-api/models"
	"io"
	"log"
	"net/http"
)

func Login(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)

	if err != nil {
		http.Error(w, "Unable to read request body", http.StatusBadRequest)
		return
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Println(err)
		}
	}(r.Body)

	var data config.Login

	if err := json.Unmarshal(body, &data); err != nil {
		http.Error(w, "Unable to parse JSON", http.StatusBadRequest)
		return
	}

	if err := data.Validate(); err != nil {
		http.Error(w, "Validation error: "+err.Error(), http.StatusBadRequest)
		return
	}

	user, err := models.AuthUser(data.Email, data.Password)

	if err != nil {
		http.Error(w, "Impossible to login", http.StatusBadRequest)
		return
	}

	token, err := config.GenerateJWT(user.Email)

	if err != nil {
		http.Error(w, "Unable to createToken", http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(token)
}

func Register(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)

	if err != nil {
		http.Error(w, "Unable to read request body", http.StatusBadRequest)
		return
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Println(err)
		}
	}(r.Body)

	var data config.Register

	if err := json.Unmarshal(body, &data); err != nil {
		http.Error(w, "Unable to parse JSON", http.StatusBadRequest)
		return
	}

	if err := data.Validate(); err != nil {
		http.Error(w, "Validation error: "+err.Error(), http.StatusBadRequest)
		return
	}

	user := models.User{
		Username: data.Username,
		Email:    data.Email,
		Password: data.Password,
		Roles:    []string{"user"},
	}

	if err := models.NewUser(&user); err != nil {
		http.Error(w, "Impossible to register", http.StatusUnprocessableEntity)
		return
	}

	json.NewEncoder(w).Encode(user)
}
