Dépendances :
```shell
go get github.com/lib/pq
go get github.com/joho/godotenv
go get github.com/gorilla/mux
go get golang.org/x/crypto
go get github.com/golang-jwt/jwt
```