package models

import (
	"database/sql"
	"errors"
	"example-api/config"
	"log"
	"time"
)

type Car struct {
	Id           int       `json:"id"`
	Manufacturer string    `json:"manufacturer"`
	Design       string    `json:"design"`
	Style        string    `json:"style"`
	Doors        uint8     `json:"doors"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}

type Cars []Car

func NewCar(c *Car) error {
	if c == nil {
		return errors.New("car is nil")
	}

	c.CreatedAt = time.Now()
	c.UpdatedAt = time.Now()

	err := config.Db().QueryRow("INSERT INTO cars (manufacturer, design, style, doors, created_at, updated_at) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id;", c.Manufacturer, c.Design, c.Style, c.Doors, c.CreatedAt, c.UpdatedAt).Scan(&c.Id)

	if err != nil {
		return errors.New("car not create")
	}

	return nil
}

func FindCarById(id int) (*Car, error) {
	var car Car

	row := config.Db().QueryRow("SELECT * FROM cars WHERE id = $1;", id)
	err := row.Scan(&car.Id, &car.Manufacturer, &car.Design, &car.Style, &car.Doors, &car.CreatedAt, &car.UpdatedAt)

	if err != nil {
		return nil, errors.New("car not found")
	}

	return &car, nil
}

func AllCars() (*Cars, error) {
	var cars Cars

	rows, err := config.Db().Query("SELECT * FROM cars")

	if err != nil {
		return nil, errors.New("car not found")
	}

	// Close rows after all readed
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			log.Println(err)
		}
	}(rows)

	for rows.Next() {
		var c Car
		err := rows.Scan(&c.Id, &c.Manufacturer, &c.Design, &c.Style, &c.Doors, &c.CreatedAt, &c.UpdatedAt)

		if err != nil {
			return nil, errors.New("car not parse")
		}

		cars = append(cars, c)
	}

	return &cars, nil
}

func UpdateCar(car *Car) error {
	car.UpdatedAt = time.Now()
	stmt, err := config.Db().Prepare("UPDATE cars SET manufacturer=$1, design=$2, style=$3, doors=$4, updated_at=$5 WHERE id=$6;")

	if err != nil {
		return errors.New("car not update")
	}

	_, err = stmt.Exec(car.Manufacturer, car.Design, car.Style, car.Doors, car.UpdatedAt, car.Id)

	if err != nil {
		return errors.New("car not parse")
	}

	return nil
}

func DeleteCarById(id int) error {
	stmt, err := config.Db().Prepare("DELETE FROM cars WHERE id=$1;")

	if err != nil {
		return errors.New("cat not deleted")
	}

	_, err = stmt.Exec(id)

	return err
}
