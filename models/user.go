package models

import (
	"encoding/json"
	"errors"
	"example-api/config"
	"time"
)

type User struct {
	Id        int       `json:"id"`
	Username  string    `json:"username"`
	Email     string    `json:"email"`
	Roles     []string  `json:"roles"`
	Password  string    `json:"-"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type Users []User

func NewUser(u *User) error {
	if u == nil {
		return errors.New("user is nil")
	}

	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()

	hashedPassword, err := config.HashPassword(u.Password)
	if err != nil {
		return err
	}

	roles, _ := json.Marshal(u.Roles)
	err = config.Db().QueryRow("INSERT INTO users (username, email, roles, password, created_at, updated_at) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id;", u.Username, u.Email, roles, hashedPassword, u.CreatedAt, u.UpdatedAt).Scan(&u.Id)

	if err != nil {
		return err
	}

	return nil
}

func AuthUser(email, password string) (*User, error) {
	var (
		user  User
		roles []uint8
	)

	row := config.Db().QueryRow("SELECT * FROM users WHERE email=$1;", email)
	err := row.Scan(&user.Id, &user.Username, &user.Email, &roles, &user.Password, &user.CreatedAt, &user.UpdatedAt)

	if err != nil {
		return nil, errors.New("user is nil")
	}

	json.Unmarshal(roles, &user.Roles)

	if !config.CheckPasswordHash(password, user.Password) {
		return nil, errors.New("invalid credentials")
	}

	return &user, nil
}
