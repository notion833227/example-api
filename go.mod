module example-api

go 1.22.4

require (
	github.com/gorilla/mux v1.8.1
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.9
	golang.org/x/crypto v0.25.0
)

require github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
